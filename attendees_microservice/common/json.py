from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}
    # mirrors whats found in LocationDetailEncoder
    def default(self, o):
        # if object to decode is same class as what is in model property
        # check if object input 'o' = instance (isistance) of class in self.model property
        if isinstance(o, self.model):
            # crete an empty dict to hold property names as keys, property values as values
            d = {}
            # handle href in list views
            # if o has get_api_url attribute
            # add return value to dictionary
            # value=o.get_api_url()
            # key='href'
            if hasattr(o, "get_api_url"):
                d["href"] = o.get_api_url()
            # iterate over self.properties (properties declared in detailencoder like ConferenceDetailEncoder)
            # if there is an error here it will impact detailencoder - impacts method
            for property_name in self.properties:
                # Use getattr fxn to get value of property of object by name
                value = getattr(o, property_name)
                if property_name in self.encoders:
                    # applies to encoders with nested encoders
                    encoder = self.encoders[property_name]
                    value = encoder.default(value)
                # put value in dict w/ property name
                d[property_name] = value
            d.update(self.get_extra_data(o))
            return d
        else:
            # boilerplate code from JSON documentation
            return super().default(o)

    # returns empty dict to dweal with edge cases
    # allows for the addition of extra data
    def get_extra_data(self, o):
        return {}
