from django.http import JsonResponse
from .models import Presentation, Status
from common.json import ModelEncoder
from events.models import Conference
from events.api_views import ConferenceListEncoder
from django.views.decorators.http import require_http_methods
import json
import pika

class StatusListEncoder(ModelEncoder):
    model = Status
    properties = ["name"]


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = ["presenter_name", "status", "conference"]
    encoders = {
        "status": StatusListEncoder(),
        "conference": ConferenceListEncoder(),
    }


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "status",
        "conference",
    ]
    encoders = {
        "status": StatusListEncoder(),
        "conference": ConferenceListEncoder(),
    }
    # def get_extra_data(self, o):
    # return {"status": o.status.name}


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        try:
            presentations = Presentation.objects.filter(
                conference_id=conference_id
            )
            return JsonResponse(
                presentations, PresentationListEncoder, safe=False
            )
        except presentations.DoesNotExist:
            return JsonResponse(
                {"alert": "conference does not exist"},
                status=400,
            )
    else:
        content = json.loads(request.body)
        try:
            content["conference"] = Conference.objects.get(id=conference_id)
        except Conference.DoesNotExist:
            return JsonResponse(
                {"alert": "Conference does not exist"}, status=400
            )
        presentations = Presentation.create(**content)
        return JsonResponse(presentations, PresentationListEncoder, safe=False)


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_presentation(request, id):
    if request.method == "GET":
        try:
            presentation = Presentation.objects.filter(id=id)
            print(presentation)
            return JsonResponse(
                presentation, PresentationDetailEncoder, safe=False
            )
        except Presentation.DoesNotExist:
            return JsonResponse({"alert": "no presenations exist"}, status=400)
    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"Presentation deleted": count > 0})
    else:
        try:
            content = json.loads(request.body)
            try:
                content["status"] = Status.objects.get(name=content["status"])
            except Status.DoesNotExist:
                return JsonResponse(
                    {"alert": "status does not exist"},
                    status=400,
                )
            Presentation.objects.filter(id=id).update(**content)
            presentation = Presentation.objects.get(id=id)
            return JsonResponse(
                presentation, PresentationDetailEncoder, safe=False
            )
        except Presentation.DoesNotExist:
            return JsonResponse(
                {"alert": "presentation does not exist"},
                status=400,
            )

def PostQ(presentation, queue):
    parameters=pika.ConnectionParameters(host="rabbitmq")
    connection=pika.BlockingConnection(parameters)
    channel=connection.channel()
    channel.queue_declare(queue=queue)
    body={
        "presenter_name":presentation.presenter_name,
        "presenter_email":presentation.presenter_email,
        "presentation_title":presentation.title,
        "presentation_status":presentation.status.name,
    }
    request_body=json.dumps(body)
    if queue=="approve_presentation":
        channel.basic_publish(
            exchange="",
            routing_key='approve_presentation',
            body=request_body,
        )
    elif queue=="reject_presentation":
        channel.basic_publish(
            exchange="",
            routing_key='reject_presentation',
            body=request_body,
        )
    connection.close()


@require_http_methods(["PUT"])
def api_approve_presentation(request,id):
    presentations=Presentation.objects.get(id=id)
    presentations.status_approve()
    PostQ(presentations,'approve_presentation')
    return JsonResponse(
        presentations,
        encoder=PresentationDetailEncoder,
        safe=False,
    )
@require_http_methods(["PUT"])
def api_reject_presentation(request,id):
    presentations=Presentation.objects.get(id=id)
    presentations.status_rejection()
    PostQ(presentations,'reject_presentation')
    return JsonResponse(
        presentations,
        encoder=PresentationDetailEncoder,
        safe=False,
    )
