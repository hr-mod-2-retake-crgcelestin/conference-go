import json
import pika
import django
import os
import sys
from django.core.mail import send_mail
from pika.exceptions import AMQPConnectionError
import time

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

#send_mail format
#consumer code
parameters = pika.ConnectionParameters(host='rabbitmq')
connection = pika.BlockingConnection(parameters)
channel = connection.channel()

def process_message(ch, method, properties, body):
    print("  Received %r" % body)
    presentation=json.loads(body)
    print(presentation)
    if presentation['presentation_status']=="APPROVED":
        send_mail(
            'Your Presentation Has Been Accepted',
            f"{presentation['presenter_name']},we\'re happy to tell you that your presentation {presentation['presentation_title']} has been accepted",
            'admin@conference.go',
            [presentation['presenter_email']],
            fail_silently=False,
        )
    elif presentation['presentation_status']=="REJECTED":
        send_mail(
            'Your presentation has been rejected',
            f"{presentation['presenter_name']}, unfortunately your presentation {presentation['presentation_title']} has been rejected",
            'admin@conference.go',
            [presentation['presenter_email']],
            fail_silently=False,
        )
while True:
    try:
        channel.queue_declare(queue='reject_presentation')
        channel.basic_consume(
            queue='reject_presentation',
            on_message_callback=process_message,
            auto_ack=True,
        )
        channel.queue_declare(queue='approve_presentation')
        channel.basic_consume(
            queue='approve_presentation',
            on_message_callback=process_message,
            auto_ack=True,
        )
        channel.start_consuming()
    except AMQPConnectionError:
        print('could not connect to rabbitmq')
        time.sleep(2.0)
